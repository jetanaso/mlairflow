from datetime import timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.utils.dates import days_ago

from src.get_initial_data import pull_data
from src.pre_process import preprocess
from src.train import choose_best_model

####################################################################################################
# Main dags

default_args = {
    "owner": "airflow",
    "wait_for_downstream": False,
    "depends_on_past": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=1),
    "email_on_failure": False,
    "email_on_retry": False,
    "email": ["test@gmail.com"],
    "provide_context": True
}

with DAG(
    default_args=default_args,
    dag_id="pretrain",
    description="pretrain initial model",
    start_date=days_ago(1),
    schedule_interval=None,
    catchup=False,
) as dag:
    start = DummyOperator(
        task_id="start"
    )

    task1 = PythonOperator(
        task_id="pull_initial_data",
        python_callable=pull_data,
    )

    task2 = PythonOperator(
        task_id="preprocess",
        python_callable=preprocess,
        op_kwargs={"pretrain": True},
    )

    task3 = PythonOperator(
        task_id="pretrain",
        python_callable=choose_best_model,
        op_kwargs={"pretrain": True},
    )

    end = DummyOperator(
        task_id="end"
    )

    start >> task1 >> task2 >> task3 >> end

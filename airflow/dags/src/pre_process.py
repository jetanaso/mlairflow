import pandas as pd

def preprocess(pretrain: bool, **kwargs):
    ti = kwargs["ti"]
    if pretrain is True:
        data = ti.xcom_pull(task_ids="pull_initial_data")
    else:
        data = ti.xcom_pull(task_ids="pull_new_data")
        
    dataframe = pd.read_json(data, orient="index")
    
    cols = ["maxtempC", "mintempC", "cloudcover", "humidity", "tempC", "sunHour",
            "HeatIndexC", "precipMM", "pressure", "windspeedKmph"]
    dataframe = dataframe[cols].copy()

    dataframe = dataframe.fillna("ffill")

    return dataframe.to_json(orient="index")

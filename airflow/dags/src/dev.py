import os
import glob
import synthia as syn
import pandas as pd
from rich import print

# data = pd.read_csv("c:/projects/mlairflow/data/initial_data/initial_data.csv")
# data = data.drop(columns=["moonrise", "moonset", "sunrise", "sunset"])
# print(data.shape)

# generator = syn.CopulaDataGenerator()     
# parameterizer = syn.QuantileParameterizer(n_quantiles=100)       
# generator.fit(data, copula=syn.GaussianCopula(), parameterize_by=parameterizer)

# samples = generator.generate(n_samples=50000, uniformization_ratio=1, stretch_factor=1)    
# synthetic = pd.DataFrame(samples, columns=data.columns)
# synthetic = synthetic[synthetic["tempC"].between(synthetic["mintempC"], synthetic["maxtempC"])].reset_index(drop=True)
# print(synthetic.shape)
# print(synthetic[:5])

folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "archive")
file_type = r"\*csv"
files = glob.glob(folder + file_type)
latest_file = max(files, key=os.path.getctime)
print(latest_file)

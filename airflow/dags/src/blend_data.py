import os
import glob
import pandas as pd


def blend_data():
    # get new data
    folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "archive", "*")
    files = glob.glob(folder)
    latest_file = max(files, key=os.path.getctime)
    new_df = pd.read_csv(latest_file)

    # get last data
    folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "in_use")

    if not os.listdir(folder): # check if directory "in_use" is empty
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "initial_data", "initial_data.csv")
        last_df = pd.read_csv(path)
    else:
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "in_use", "current.csv")
        last_df = pd.read_csv(path)
    
    # blend old and new data
    new_nrows = len(new_df)
    last_df = last_df[new_nrows:].copy()
    final_df = pd.concat([last_df, new_df], ignore_index=True)
    
    destination = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "in_use", "current.csv")
    final_df.to_csv(destination, index=False)

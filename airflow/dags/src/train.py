import os
import pickle
import pytz
from datetime import datetime
from dotenv import load_dotenv
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
import mlflow

load_dotenv()
MLFLOW_TRACKING_URI = os.getenv("MLFLOW_TRACKING_URI")
MLFLOW_S3_ENDPOINT_URL = os.getenv("MLFLOW_S3_ENDPOINT_URL")
AWS_ACCESS_KEY_ID = os.getenv("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")


def choose_best_algorithm(dataframe) -> dict:
    X = dataframe.drop(columns=["tempC"]).values
    y = dataframe["tempC"].values
    kfold = KFold(shuffle=True, n_splits=5)

    models = {"LR": LinearRegression(),
              "DT": DecisionTreeRegressor(),
              "RF": RandomForestRegressor()}

    results = {}
    for _, model_obj in models.items():
        avg_score = np.mean(cross_val_score(model_obj, X, y, cv=kfold, n_jobs=-1))
        results[model_obj] = avg_score

    best_algorithm = max(results, key=results.get)
    print(results)
    print("The best algorithm is", best_algorithm)
    print("The best score is", results[best_algorithm])

    return best_algorithm

def choose_best_model(pretrain: bool, **kwargs) -> None:
    ti = kwargs["ti"]
    data = ti.xcom_pull(task_ids="preprocess")
    dataframe = pd.read_json(data, orient="index")
    
    X = dataframe.drop(columns=["tempC"]).values
    y = dataframe["tempC"].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=0)
    
    param_n_estimator = [100, 500]
    param_max_depth = [3, 7]
    combinations = [(x, y) for x in param_n_estimator for y in param_max_depth]
    
    mlflow.set_tracking_uri("http://mlflow-server:5005")
    if pretrain is True:
        mlflow.set_experiment("pretrain")
    else:
        mlflow.set_experiment(f"retrain")
        
    mlflow.autolog(log_model_signatures=True, log_input_examples=True)

    rmse = 999999
    now = datetime.now(tz=pytz.timezone("Asia/Bangkok"))
    dt_string = now.strftime("%d%m%Y_%H%M%S")
    with mlflow.start_run(run_name=dt_string) as run:
        for n_estimator, max_depth in combinations:
            with mlflow.start_run(run_name=f"{n_estimator} {max_depth}", nested=True) as nested:
                model = RandomForestRegressor(n_estimators=n_estimator, max_depth=max_depth)
                model.fit(X_train, y_train)

                y_pred = model.predict(X_test)
                _mae = mean_absolute_error(y_test, y_pred)
                _rmse = np.sqrt(mean_squared_error(y_test, y_pred))
                _r2 = r2_score(y_test, y_pred)

                mlflow.log_metrics({"testing_rmse": _rmse,
                                    "testing_r2": _r2,
                                    "testing_mae": _mae})

                # save the model
                path_archive = os.path.join(os.path.dirname(__file__), "..", "..", "..", "models", "archive", dt_string+".pickle")
                pickle.dump(model, open(path_archive, "wb"))

                if _rmse < rmse:
                    path_in_use = os.path.join(os.path.dirname(__file__), "..", "..", "..", "models", "in_use", "current_model.pickle")
                    pickle.dump(model, open(path_in_use, "wb"))
                    rmse = _rmse

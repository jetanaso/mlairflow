import os
import pandas as pd


# url = "https://raw.githubusercontent.com/neetika6/Machine-Learning-Model-for-Weather-Forecasting/main/kanpur.csv"
path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "initial_data", "initial_data.csv")
print(path)

def pull_data() -> pd.DataFrame:
    dataframe = pd.read_csv(path)
    return dataframe.to_json(orient="index")

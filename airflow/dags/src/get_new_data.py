import os
import synthia as syn
import pytz
from datetime import datetime
import pandas as pd


def pull_new_data() -> pd.DataFrame:
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "initial_data", "initial_data.csv")
    data = pd.read_csv(path)
    data = data.drop(columns=["moonrise", "moonset", "sunrise", "sunset"])    

    generator = syn.CopulaDataGenerator()     
    parameterizer = syn.QuantileParameterizer(n_quantiles=100)       
    generator.fit(data, copula=syn.GaussianCopula(), parameterize_by=parameterizer)

    samples = generator.generate(n_samples=10000, uniformization_ratio=1, stretch_factor=1)    
    synthetic = pd.DataFrame(samples, columns=data.columns)
    synthetic = synthetic[synthetic["tempC"].between(synthetic["mintempC"], synthetic["maxtempC"])].reset_index(drop=True)
    
    # save data
    now = datetime.now(tz=pytz.timezone("Asia/Bangkok"))
    dt_string = now.strftime(f"%d%m%Y_%H%M%S")
    destination = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "..", "data", "archive", f"{dt_string}.csv")
    synthetic.to_csv(destination, index=False)

    return synthetic.to_json(orient="index")

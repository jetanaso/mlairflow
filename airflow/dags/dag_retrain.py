from datetime import timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy import DummyOperator
from airflow.utils.dates import days_ago

from src.get_new_data import pull_new_data
from src.blend_data import blend_data
from src.pre_process import preprocess
from src.train import choose_best_model

####################################################################################################
# Main dags

default_args = {
    "owner": "airflow",
    "wait_for_downstream": False,
    "depends_on_past": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=1),
    "email_on_failure": False,
    "email_on_retry": False,
    "email": ["test@gmail.com"],
    "provide_context": True
}

with DAG(
    default_args=default_args,
    dag_id="retrain",
    description="retrain model when new data coming in",
    start_date=days_ago(1),
    schedule_interval=timedelta(minutes=1),
    catchup=False,
) as dag:
    start = DummyOperator(
        task_id="start"
    )

    task1 = PythonOperator(
        task_id="pull_new_data",
        python_callable=pull_new_data,
    )
    
    task2 = PythonOperator(
        task_id="blend_old_and_new_data",
        python_callable=blend_data,
    )

    task3 = PythonOperator(
        task_id="preprocess",
        python_callable=preprocess,
        op_kwargs={"pretrain": False},
    )

    task4 = PythonOperator(
        task_id="retrain",
        python_callable=choose_best_model,
        op_kwargs={"pretrain": False},
    )

    end = DummyOperator(
        task_id="end"
    )

    start >> task1 >> task2 >> task3 >> task4 >> end
